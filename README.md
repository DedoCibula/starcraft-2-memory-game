# Starcraft 2 Memory Game #

An adaptation of popular Memory game created as a tribute to one of Blizzard's most popular real-time strategy games - Starcraft 2.

### Technologies and Patters ###

* WPF (Windows Presentation Foundation) desktop application
* Reactive Extensions (Rx)
* Extended WPF Toolkit
* MVVM and Observer design patterns