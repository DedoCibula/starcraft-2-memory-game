﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Diagnostics;

namespace Pexeso.Cards
{
    public class Card : ObservableObject
    {
        public Card FellowCard { get; set; }
        public CardType CardType { get; private set; }

        private string _imageSource = @"pack://application:,,,/Images/Back.jpg";
        public string ImageSource
        {
            get
            {
                return _imageSource;
            }

            set
            {
                if (_imageSource == value) return;

                _imageSource = value;
                RaisePropertyChanged("ImageSource");
            }
        }

        private string _imageBack;
        public string ImageBack
        {
            get
            {
                return _imageBack;
            }

            set
            {
                if (_imageBack == value) return;

                _imageBack = value;
                RaisePropertyChanged("ImageBack");
            }
        }

        public void FlipCard()
        {
            string temp = ImageBack;
            ImageBack = ImageSource;
            ImageSource = temp;
        }

        public Card(CardType cardType)
        {
            try
            {
                ImageBack = string.Format(@"pack://application:,,,/Images/{0}.jpg" , cardType);
                this.CardType = cardType;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Couldn't locate a picture", ex.Message, ex.Source);
            }
        }
    }
}
