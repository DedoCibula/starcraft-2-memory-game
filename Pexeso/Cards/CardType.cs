﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pexeso.Cards
{
    public enum CardType
    {
        Zergling,
        Marine,
        Zealot,
        Marauder,
        Hydralisk,
        Immortal,
        Hellion,
        Queen,
        Phoenix,
        Ghost,
        Mutalisk,
        Banshee,
        Carrier,
        Ultralisk,
        Battlecruiser,
        Mothership
    }
}
