﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pexeso.Views;

namespace Pexeso.Cards
{
    static class CardSetter
    {
        public static CardVisualizer[,] ShuffleCards(this IDictionary<CardVisualizer, Card> cards)
        {
            if (cards == null)
                throw new ArgumentNullException("Argument cards was null");
            List<CardVisualizer> shuffledCards = new List<CardVisualizer>(cards.Keys);
            var temp = CreateField(cards.Count);
            Random rand = new Random((int)DateTime.Now.Ticks);
            for (int i = 0; i <= temp.GetUpperBound(0); i++)
                for (int j = 0; j <= temp.GetUpperBound(1); j++)
                {
                    int x = rand.Next(0, shuffledCards.Count - 1);
                    temp[i, j] = shuffledCards[x];
                    shuffledCards.RemoveAt(x);
                    int y = temp.GetUpperBound(1);
                }
            return temp;
        }

        public static int AssignRow(this CardVisualizer[,] cards, int rowCount)
        {
            if (cards == null)
                throw new ArgumentNullException("cards");
            if (rowCount <= 0)
                throw new ArgumentException("rowCount cannot be zero or negative number");
            return (rowCount - (cards.GetUpperBound(0) + 1)) / 2;
        }

        public static int AssignColumn(this CardVisualizer[,] cards, int columnCount)
        {
            if (cards == null)
                throw new ArgumentNullException("cards");
            if (columnCount <= 0)
                throw new ArgumentException("columnCount cannot be zero or negative number");
            return (columnCount - (cards.GetUpperBound(1) + 1)) / 2;
        }

        private static CardVisualizer[,] CreateField(int area)
        {
            if (area < 2 || area > 32)
                throw new ArgumentOutOfRangeException("Area is too " + (area < 2 ? "small" : "large"));
            int temp = (int)Math.Floor(Math.Sqrt(area));
            while (temp > 1)
            {
                if (area % temp == 0)
                    break;
                temp--;
            }
            return new CardVisualizer[temp, area / temp];
        }
    }
}
