﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reactive.Concurrency;
using System.Reactive;
using System.Threading;
using Pexeso.Cards;

namespace Pexeso.Views
{
    /// <summary>
    /// Interaction logic for CardDeck.xaml
    /// </summary>
    public partial class CardDeck : UserControl
    {
        private Subject<bool> isMatch;
        private IDictionary<CardVisualizer, Card> cards;
        private IObservable<CardVisualizer> cardClicks;
        private IDisposable outerDisposer = System.Reactive.Disposables.Disposable.Empty;
        private IDisposable innerDisposer = System.Reactive.Disposables.Disposable.Empty;
        private IDisposable matchDisposer = System.Reactive.Disposables.Disposable.Empty;
        private bool ignoreNextCard;
        
        public Subject<Unit> DeckIsEmpty { get; private set; }
        public Subject<Unit> StartTimer { get; private set; }

        public CardDeck(Subject<bool> status)
        {
            if (status == null)
                throw new ArgumentNullException("status");
            InitializeComponent();
            InitializeDeck(status);
            
            cardClicks = Observable.FromEventPattern<MouseButtonEventArgs>(cardDeck, "MouseLeftButtonDown")
                                   .Select(ev => ev.EventArgs.Source as CardVisualizer)
                                   .Where(ev => ev != null);   
        }

        public void SetDefault()
        {
            matchDisposer.Dispose();
            innerDisposer.Dispose();
            outerDisposer.Dispose();
            cardDeck.Children.Clear();
            cards.Clear();
            cardDeck.IsHitTestVisible = true;
        }

        public void CreateCardInDeck(Card card)
        {
            if (card == null)
                throw new ArgumentException("Argument card was null");

            Card temp = new Card(card.CardType);
            temp.FellowCard = card;
            card.FellowCard = temp;
            var cardVisual = new CardVisualizer { DataContext = card };
            var fellowCardVisual = new CardVisualizer { DataContext = temp };

            cards.Add(cardVisual, card);
            cards.Add(fellowCardVisual, temp);
        }

        public void DrawDeck()
        {
            var element = cards.ShuffleCards();
            for (int i = 0; i <= element.GetUpperBound(0); i++)
                for (int j = 0; j <= element.GetUpperBound(1); j++)
                {
                    Grid.SetRow(element[i, j], i + element.AssignRow(cardDeck.RowDefinitions.Count));
                    Grid.SetColumn(element[i, j], j + element.AssignColumn(cardDeck.ColumnDefinitions.Count));
                    cardDeck.Children.Add(element[i, j]);
                }

            StartCardClicks();
        }

        public void GameOver()
        {
            cardDeck.IsHitTestVisible = false;
            foreach (var card in cards.Keys)
            {
                if (!card.isFlipped)
                    card.ShowCardValue();
            }
            ignoreNextCard = false;
        }

        private void StartCardClicks()
        {
            var time = 0;
            outerDisposer = cardClicks
                      .SkipWhile(c => c.mainCanvas.Opacity < 1)
                      .Do(_ => Scheduler.TaskPool.Schedule(() =>
                          {
                              if (time++ == 0)
                                  StartTimer.OnNext(Unit.Default);
                          }))
                      .Where(_ => !ignoreNextCard)
                      .Subscribe(first =>
                      {
                          first.ShowCardValue();
                          ignoreNextCard = true;

                          FixedPoint(CheckNext => innerDisposer = cardClicks.Take(1).Subscribe(second =>
                          {
                              if (first.Equals(second))
                                  CheckNext();
                              else
                              {
                                  second.ShowCardValue();
                                  Scheduler.TaskPool.Schedule(() => isMatch.OnNext(cards[first].FellowCard.Equals(cards[second])));
                                  EvaluateCards(first, second);
                              }
                          }));
                      });
        }


        private void EvaluateCards(CardVisualizer first, CardVisualizer second)
        {
            matchDisposer = Observable.Delay(Observable.Return(1), TimeSpan.FromSeconds(1))
                                      .ObserveOnDispatcher()
                                      .Subscribe(_ =>
                                      {
                                          ignoreNextCard = false;

                                          if (cards[first].FellowCard.Equals(cards[second]))
                                          {
                                              cards.Remove(first);
                                              cardDeck.Children.Remove(first);
                                              cards.Remove(second);
                                              cardDeck.Children.Remove(second);
                                              Scheduler.TaskPool.Schedule(() =>
                                              {
                                                  if (cards.Count == 0)
                                                      DeckIsEmpty.OnNext(Unit.Default);
                                              });
                                          }
                                          else
                                          {
                                              first.TurnCardBack();
                                              second.TurnCardBack();
                                          }
                                      });
        }

        private void InitializeDeck(Subject<bool> status)
        {
            this.DataContext = this;
            cards = new Dictionary<CardVisualizer, Card>();
            isMatch = status;
            DeckIsEmpty = new Subject<Unit>();
            StartTimer = new Subject<Unit>();
            ignoreNextCard = false;

            for (int i = 0; i < 4; i++)
                cardDeck.RowDefinitions.Add(new RowDefinition { Height = new GridLength(150) });

            for (int i = 0; i < 8; i++)
                cardDeck.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(120) });
        }
        
        private void FixedPoint(Action<Action> function)
        {
            function(() => FixedPoint(function));
        }
    }
}
