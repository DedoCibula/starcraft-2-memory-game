﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Windows.Media.Animation;
using Pexeso.Cards;

namespace Pexeso.Views
{
    /// <summary>
    /// Interaction logic for CardVisualizer.xaml
    /// </summary>
    public partial class CardVisualizer : UserControl
    {
        ResourceDictionary resource;
        public bool isFlipped { get; private set; }

        public CardVisualizer()
        {
            InitializeComponent();
            resource = mainCanvas.Resources;
        }

        public void ShowCardValue()
        {
            mainCanvas.Resources = null;
            FlipCard();
            isFlipped = true;
        }

        public void TurnCardBack()
        {
            FlipCard();
            mainCanvas.Resources = resource;
            EaseBack();
            isFlipped = false;
        }

        private void FlipCard()
        {
            var blink = new DoubleAnimation(0, TimeSpan.FromSeconds(0.1));
            var flip = new DoubleAnimation(120, 0, TimeSpan.FromSeconds(0.1)) { EasingFunction = new SineEase { EasingMode = EasingMode.EaseOut } };
            var marginDown = new ThicknessAnimation(new Thickness(0, 0, 0, 0), TimeSpan.Zero);
            var heightFront = new DoubleAnimation(150, TimeSpan.Zero);
            var onClick = new Storyboard { Children = new TimelineCollection(new List<Timeline> { blink, flip, heightFront, marginDown }), AutoReverse = true };
            Storyboard.SetTarget(blink, mainCanvas);
            Storyboard.SetTargetProperty(blink, new PropertyPath(Canvas.OpacityProperty));
            Storyboard.SetTarget(flip, mainCanvas);
            Storyboard.SetTargetProperty(flip, new PropertyPath(Canvas.WidthProperty));
            Storyboard.SetTarget(marginDown, mainCanvas);
            Storyboard.SetTargetProperty(marginDown, new PropertyPath(Canvas.MarginProperty));
            Storyboard.SetTarget(heightFront, mainCanvas);
            Storyboard.SetTargetProperty(heightFront, new PropertyPath(Canvas.HeightProperty));
            onClick.Begin(mainCanvas, HandoffBehavior.SnapshotAndReplace);
            (DataContext as Card).FlipCard();
        }

        private void EaseBack()
        {
            var widthBack = new DoubleAnimation(100, TimeSpan.FromSeconds(0.5));
            var heightBack = new DoubleAnimation(130, TimeSpan.FromSeconds(0.5));
            var marginUp = new ThicknessAnimation(new Thickness(0, 0, 0, 0), TimeSpan.FromSeconds(0.5)); 
            var onClick = new Storyboard { Children = new TimelineCollection(new List<Timeline> { widthBack, heightBack, marginUp }) };
            Storyboard.SetTarget(widthBack, mainCanvas);
            Storyboard.SetTargetProperty(widthBack, new PropertyPath(Canvas.WidthProperty));
            Storyboard.SetTarget(heightBack, mainCanvas);
            Storyboard.SetTargetProperty(heightBack, new PropertyPath(Canvas.HeightProperty));
            Storyboard.SetTarget(marginUp, mainCanvas);
            Storyboard.SetTargetProperty(marginUp, new PropertyPath(Canvas.MarginProperty));
            onClick.Begin(mainCanvas, HandoffBehavior.Compose);
        }
    }
}
