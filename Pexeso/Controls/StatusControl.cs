﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Windows.Media;

namespace Pexeso.Controls
{
    class StatusControl : ObservableObject
    {
        public Subject<bool> IsMatch { get; set; }

        private string _imageUri = null;
        public string ImageUri
        {
            get
            {
                return _imageUri;
            }

            set
            {
                if (_imageUri == value) return;

                _imageUri = value;
                RaisePropertyChanged("ImageUri");
            }
        }

        private string _message = null;
        public string Message
        {
            get
            {
                return _message;
            }

            set
            {
                if (_message == value) return;

                _message = value;
                FontColor = (value == "MATCH") ? Brushes.Green : Brushes.Red;
                RaisePropertyChanged("Message");
            }
        }

        private Brush _fontColor = Brushes.Green;
        public Brush FontColor
        {
            get
            {
                return _fontColor;
            }

            set
            {
                if (_fontColor == value) return;

                _fontColor = value;
                RaisePropertyChanged("FontColor");
            }
        }

        public StatusControl()
        {
            IsMatch = new Subject<bool>();

            IsMatch
                .Throttle(TimeSpan.Zero)
                .Do(b =>
                {
                    ImageUri = b ? @"pack://application:,,,/Images/GreenCheck.jpg" : @"pack://application:,,,/Images/RedCross.jpg";
                    Message = b ? @"MATCH" : @"WRONG";
                })
                .Throttle(TimeSpan.FromSeconds(1))
                .Subscribe(b =>
                {
                    ImageUri = null;
                    Message = null;
                });
        }
    }
}
