﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Threading;
using System.Diagnostics;
using System.Windows.Threading;
using Pexeso.Windows;
using Pexeso.Views;
using Pexeso.Levels;

namespace Pexeso.Controls
{
    class MainViewModel
    {
        public MainWindow boundWindow;
        public CardDeck CardDeck { get; private set; }
        public ProgressBarControl ProgressBarControl { get; private set; }
        public StatusControl StatusControl { get; private set; }

        private IList<Level> levels;
        private IDisposable disposer = System.Reactive.Disposables.Disposable.Empty;

        private RelayCommand _startNewGame;
        public RelayCommand StartNewGame
        {
            get
            {
                _startNewGame = _startNewGame ?? new RelayCommand(() => StartGame()) { Executable = true };
                return _startNewGame;
            }
        }

        private void StartGame()
        {
            PrepareGameLevel(0);
            CardDeck.DrawDeck();
            ToggleButtons();
        }

        private RelayCommand _abandonGame;
        public RelayCommand AbandonGame
        {
            get
            {
                _abandonGame = _abandonGame ?? new RelayCommand(() => QuitGame()) { Executable = false };
                return _abandonGame;
            }
        }

        private void QuitGame()
        {
            ProgressBarControl.PauseTimer();
            if (Xceed.Wpf.Toolkit.MessageBox.Show("Are you sure you want to quit current game?", "Please, confirm your choice", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) != MessageBoxResult.Yes)
            {
                ProgressBarControl.ResumeTimer();
                return;
            }
            PrepareGameLevel(0);
            ToggleButtons();
        }
        
        public MainViewModel(MainWindow window)
        {
            this.boundWindow = window;
            AddControls();
            levels = LevelDesigner.CreateGameLevels();
            
            CardDeck.StartTimer.Subscribe(_ => ProgressBarControl.StartTimer());
            ProgressBarControl.TimerEnded.ObserveOnDispatcher().Subscribe(_ =>
                {
                    disposer.Dispose();
                    new GameOver().ShowDialog();
                    ProgressBarControl.SetDefault();
                    CardDeck.GameOver();
                    ToggleButtons();
                });
        }

        private void PrepareGameLevel(int levelNumber)
        {
            CleanPreviousLevel();
            CardDeck.BuildLevel(levels[levelNumber]);
            disposer = CardDeck.DeckIsEmpty.ObserveOnDispatcher().Subscribe(_ =>
                {
                    ProgressBarControl.StopTimer();
                    if (levelNumber < levels.Count - 1)
                    {
                        new LevelPassed(levels[levelNumber].Number).ShowDialog();
                        PrepareGameLevel(++levelNumber);
                        CardDeck.DrawDeck();
                    }
                    else
                    {
                        if (!new Victory().ShowDialog().Value)
                            boundWindow.Close();
                        ProgressBarControl.SetDefault();
                        ToggleButtons();
                    }
                });
            ProgressBarControl.TimerSeconds = levels[levelNumber].TimerSeconds;
        }

        private void ToggleButtons()
        {
            StartNewGame.Executable = !StartNewGame.Executable;
            AbandonGame.Executable = !AbandonGame.Executable;
        }

        private void CleanPreviousLevel()
        {
            ProgressBarControl.SetDefault();
            CardDeck.SetDefault();
            disposer.Dispose();
        }

        private void AddControls()
        {
            StatusControl = new StatusControl();
            ProgressBarControl = new ProgressBarControl();
            CardDeck = new CardDeck(StatusControl.IsMatch);
            Grid.SetRow(CardDeck, 1);
            Grid.SetRowSpan(CardDeck, 2);
            this.boundWindow.mainGrid.Children.Add(CardDeck);
        }
    }
}
