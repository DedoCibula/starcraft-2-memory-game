﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;
using System.Reactive.Linq;
using System.Reactive.Concurrency;
using System.Windows.Media.Animation;
using System.Windows.Controls;
using System.Diagnostics;
using System.Threading;
using System.Reactive.Subjects;
using System.Reactive;

namespace Pexeso.Controls
{
    class ProgressBarControl : ObservableObject
    {
        private IDisposable disposer = System.Reactive.Disposables.Disposable.Empty;
        private IObservable<double> counter;

        private Subject<Unit> _timerEnded;
        public Subject<Unit> TimerEnded
        {
            get 
            {
                _timerEnded = _timerEnded ?? new Subject<Unit>();
                return _timerEnded; 
            }
        }

        private double _timerSeconds = 5;
        public double TimerSeconds
        {
            get { return _timerSeconds; }
            set 
            {
                if (value < 0)
                    throw new ArgumentException("Value cannot be below 0");
                if (value == _timerSeconds) return;
                _timerSeconds = value; 
            }
        }

        private double _progressValue = 0;
        public double ProgressValue
        {
            get
            {
                return _progressValue;
            }

            set
            {
                if (_progressValue == value) return;

                _progressValue = value;
                RaisePropertyChanged("ProgressValue");
            }
        }

        private double _progressMaximum = 101;
        public double ProgressMaximum
        {
            get
            {
                return _progressMaximum;
            }

            set
            {
                if (_progressMaximum == value) return;

                _progressMaximum = value;
                RaisePropertyChanged("ProgressMaximum");
            }
        }

        private string _message = "READY";
        public string Message
        {
            get
            {
                return _message;
            }

            set
            {
                if (_message == value) return;

                _message = value;
                RaisePropertyChanged("Message");
            }
        }

        private Brush _progressForeground = Brushes.Green;
        public Brush ProgressForeground
        {
            get
            {
                return _progressForeground;
            }

            set
            {
                if (_progressForeground == value) return;

                _progressForeground = value;
                RaisePropertyChanged("ProgressForeground");
            }
        }

        private Brush _messageForeground = Brushes.Green;
        public Brush MessageForeground
        {
            get
            {
                return _messageForeground;
            }

            set
            {
                if (_messageForeground == value) return;

                _messageForeground = value;
                RaisePropertyChanged("MessageForeground");
            }
        }

        public void SetDefault()
        {
            ProgressMaximum = 101;
            ProgressValue = 0;
            Message = "READY";
            StopTimer();
        }
        
        public void StartTimer()
        {
            counter = Observable.Generate(TimerSeconds * 20,
                                              s => s >= 0,
                                              s =>
                                              {
                                                  return s - 1;
                                              },
                                              s => s,
                                              _ => TimeSpan.FromSeconds(0.05),
                                              Scheduler.TaskPool);
            ProgressMaximum = (ProgressMaximum == 101) ? TimerSeconds * 20 : ProgressMaximum;
            ProgressValue = (ProgressValue == 0) ? ProgressMaximum : ProgressValue;
            disposer = counter
                .Zip(counter, (f, s) => new
                {
                    miliseconds = f,
                    seconds = (s + 19) / 20
                })
                .Subscribe(v =>
                {
                    if ((int)v.seconds <= 10) OnLastSeconds();
                    Message = string.Format("{0} second{1}", (int)v.seconds, (v.seconds != 1) ? "s" : "");
                    ProgressValue = v.miliseconds;
                }, () => TimerEnded.OnNext(Unit.Default));
        }

        public void PauseTimer()
        {
            disposer.Dispose();
        }

        public void ResumeTimer()
        {
            if (ProgressValue == 0) return;
            TimerSeconds = ProgressValue / 20;
            StartTimer();
        }

        public void StopTimer()
        {
            disposer.Dispose();
            ProgressForeground = Brushes.Green;
            MessageForeground = Brushes.Green;
        }

        private void OnLastSeconds()
        {
            ProgressForeground = (ProgressForeground == Brushes.Green) ? Brushes.Red : Brushes.Green;
            MessageForeground = Brushes.Red;
        }
    }
}
