﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pexeso.Views;
using Pexeso.Cards;

namespace Pexeso.Levels
{
    static class LevelDesigner
    {
        public static IList<Level> CreateGameLevels()
        {
            return new List<Level>
            {
                new Level { Number = 1, CardValueCount = 2, TimerSeconds = 5 },
                new Level { Number = 2, CardValueCount = 4, TimerSeconds = 10 },
                new Level { Number = 3, CardValueCount = 8, TimerSeconds = 30 },
                new Level { Number = 4, CardValueCount = 12, TimerSeconds = 60 },
                new Level { Number = 5, CardValueCount = 16, TimerSeconds = 90}
            };
        }

        public static void BuildLevel(this CardDeck cardDeck, Level level)
        {
            if (cardDeck == null || level == null)
                throw new ArgumentNullException(cardDeck == null ? "cardDeck" : "level");
            var cardType = CardType.Zergling;
            for (int i = 0; i < level.CardValueCount; i++)
            {
                cardDeck.CreateCardInDeck(new Card(cardType++));
            }
        }
    }
}
