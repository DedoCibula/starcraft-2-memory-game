﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pexeso.Levels
{
    class Level
    {
        public int Number { get; set; }
        public int TimerSeconds { get; set; }
        public int CardValueCount { get; set; }
    }
}
