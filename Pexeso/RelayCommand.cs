﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Pexeso
{
    class RelayCommand : ICommand
    {
        Action command;
        bool canExecute = true;
        public event EventHandler CanExecuteChanged;

        public bool Executable
        {
            get { return canExecute; }
            set
            {
                if (canExecute == value) return;
                canExecute = value;
                var handler = CanExecuteChanged;
                if (handler != null) handler(this, EventArgs.Empty);
            }
        }
        
        public bool CanExecute(object parameter)
        {
            return canExecute;
        }

        public void Execute(object parameter)
        {
            command();
        }

        public RelayCommand(Action command)
        {
            this.command = command;
        }
    }
}
